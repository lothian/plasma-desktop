# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2014, 2017.
# Vit Pelcak <vit@pelcak.org>, 2017, 2018, 2019, 2020, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-08 01:39+0000\n"
"PO-Revision-Date: 2023-06-14 14:59+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.2\n"

#: actions.cpp:19
#, kde-format
msgid "Touchpad"
msgstr "Touchpad"

#: actions.cpp:22
#, kde-format
msgid "Enable Touchpad"
msgstr "Povolit touchpad"

#: actions.cpp:30
#, kde-format
msgid "Disable Touchpad"
msgstr "Zakázat touchpad"

#: actions.cpp:38
#, kde-format
msgid "Toggle Touchpad"
msgstr "Přepnout touchpad"

#: backends/kwin_wayland/kwinwaylandbackend.cpp:59
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"Získávání informací o vstupních zařízeních se nezdařilo. Znovu otevřete "
"tento modul nastavení."

#: backends/kwin_wayland/kwinwaylandbackend.cpp:74
#, kde-format
msgid "Critical error on reading fundamental device infos for touchpad %1."
msgstr ""
"Při načítání základních informací o zařízení touchpadu %1 došlo ke kritické "
"chybě."

#: backends/x11/xlibbackend.cpp:71
#, kde-format
msgid "Cannot connect to X server"
msgstr "Nelze se připojit k  X serveru"

#: backends/x11/xlibbackend.cpp:84 kcm/libinput/touchpad.qml:94
#, kde-format
msgid "No touchpad found"
msgstr "Nebyl nalezen žádný touchpad"

#: backends/x11/xlibbackend.cpp:124 backends/x11/xlibbackend.cpp:138
#, kde-format
msgid "Cannot apply touchpad configuration"
msgstr "Nelze použít nastavení touchpadu"

#: backends/x11/xlibbackend.cpp:152 backends/x11/xlibbackend.cpp:165
#, kde-format
msgid "Cannot read touchpad configuration"
msgstr "Nelze přečíst nastavení touchpadu"

#: backends/x11/xlibbackend.cpp:178
#, kde-format
msgid "Cannot read default touchpad configuration"
msgstr "Výchozí konfiguraci touchpadu nelze načíst"

#: kcm/libinput/touchpad.qml:108
#, kde-format
msgid "Device:"
msgstr "Zařízení:"

#: kcm/libinput/touchpad.qml:134
#, kde-format
msgid "General:"
msgstr "Obecné:"

#: kcm/libinput/touchpad.qml:135
#, kde-format
msgid "Device enabled"
msgstr "Zařízení je povolené"

#: kcm/libinput/touchpad.qml:139
#, kde-format
msgid "Accept input through this device."
msgstr "Povolit vstup skrze toto zařízení."

#: kcm/libinput/touchpad.qml:163
#, kde-format
msgid "Disable while typing"
msgstr "Zakázat při psaní"

#: kcm/libinput/touchpad.qml:167
#, kde-format
msgid "Disable touchpad while typing to prevent accidental inputs."
msgstr "Vypnout touchpad při psaní, aby nedocházelo k nechtěnému posouvání."

#: kcm/libinput/touchpad.qml:191
#, kde-format
msgid "Left handed mode"
msgstr "Režim pro leváka"

#: kcm/libinput/touchpad.qml:195
#, kde-format
msgid "Swap left and right buttons."
msgstr "Prohodit levé a pravé tlačítko."

#: kcm/libinput/touchpad.qml:219
#, kde-format
msgid "Press left and right buttons for middle click"
msgstr "Levé a pravé tlačítko je prostřední tlačítko"

#: kcm/libinput/touchpad.qml:223 kcm/libinput/touchpad.qml:864
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"Kliknutí levým a pravým tlačítkem zároveň je kliknutí prostředním tlačítkem."

#: kcm/libinput/touchpad.qml:254
#, kde-format
msgid "Pointer speed:"
msgstr "Rychlost ukazatele:"

#: kcm/libinput/touchpad.qml:352
#, kde-format
msgid "Pointer acceleration:"
msgstr "Zrychlení ukazatele:"

# žádné parametry funkce v inspektoru funkcí
#: kcm/libinput/touchpad.qml:383
#, kde-format
msgid "None"
msgstr "Nic"

#: kcm/libinput/touchpad.qml:387
#, kde-format
msgid "Cursor moves the same distance as finger."
msgstr "Kurzor se pohybuje o stejnou vzdálenost jako prst."

#: kcm/libinput/touchpad.qml:396
#, kde-format
msgid "Standard"
msgstr "Standardní"

#: kcm/libinput/touchpad.qml:400
#, kde-format
msgid "Cursor travel distance depends on movement speed of finger."
msgstr "Kurzor se pohybuje v závislosti na rychlosti pohybu prstu."

#: kcm/libinput/touchpad.qml:415
#, kde-format
msgid "Tapping:"
msgstr "Ťuknutí:"

#: kcm/libinput/touchpad.qml:416
#, kde-format
msgid "Tap-to-click"
msgstr "Klik ťuknutím"

#: kcm/libinput/touchpad.qml:420
#, kde-format
msgid "Single tap is left button click."
msgstr "Jedno ťuknutí je levé tlačítko."

#: kcm/libinput/touchpad.qml:449
#, kde-format
msgid "Tap-and-drag"
msgstr "Přetažení po ťuknutí"

#: kcm/libinput/touchpad.qml:453
#, kde-format
msgid "Sliding over touchpad directly after tap drags."
msgstr "Posouvání prstu po dotyku umožňuje přetahovat objekty."

#: kcm/libinput/touchpad.qml:480
#, kde-format
msgid "Tap-and-drag lock"
msgstr "Uzamčení přetažení po ťuknutí"

#: kcm/libinput/touchpad.qml:484
#, kde-format
msgid "Dragging continues after a short finger lift."
msgstr "Krátké zvednutí prstu nepřeruší přetažení."

#: kcm/libinput/touchpad.qml:504
#, kde-format
msgid "Two-finger tap:"
msgstr "Ťuknutí dvěma prsty:"

#: kcm/libinput/touchpad.qml:515
#, kde-format
msgid "Right-click (three-finger tap to middle-click)"
msgstr "Pravé tlačítko (dotyk třemi prsty je prostřední tlačítko)"

#: kcm/libinput/touchpad.qml:516
#, kde-format
msgid ""
"Tap with two fingers to right-click, tap with three fingers to middle-click."
msgstr ""
"Ťuknutí dvěma prsty je pravé tlačítko, dotyk třemi prsty je prostřední "
"tlačítko."

#: kcm/libinput/touchpad.qml:518
#, kde-format
msgid "Middle-click (three-finger tap right-click)"
msgstr "Prostřední tlačítko (dotyk třemi prsty je pravé tlačítko)"

#: kcm/libinput/touchpad.qml:519
#, kde-format
msgid ""
"Tap with two fingers to middle-click, tap with three fingers to right-click."
msgstr ""
"Dotyk dvěma prsty je prostřední tlačítko, dotyk třemi prsty je pravé "
"tlačítko."

#: kcm/libinput/touchpad.qml:521
#, kde-format
msgid "Right-click"
msgstr "Kliknutí pravým tlačítkem myši"

#: kcm/libinput/touchpad.qml:522
#, kde-format
msgid "Tap with two fingers to right-click."
msgstr "Ťuknutí dvěma prsty je kliknutí pravým tlačítkem."

#: kcm/libinput/touchpad.qml:524
#, kde-format
msgid "Middle-click"
msgstr "Kliknutí prostředním tlačítkem myši"

#: kcm/libinput/touchpad.qml:525
#, kde-format
msgid "Tap with two fingers to middle-click."
msgstr "Ťuknutí dvěma prsty je kliknutí prostředním tlačítkem."

#: kcm/libinput/touchpad.qml:584
#, kde-format
msgid "Scrolling:"
msgstr "Rolování:"

#: kcm/libinput/touchpad.qml:613
#, kde-format
msgid "Two fingers"
msgstr "Dva prsty"

#: kcm/libinput/touchpad.qml:617
#, kde-format
msgid "Slide with two fingers scrolls."
msgstr "Posun dvěma prsty roluje obrazovku."

#: kcm/libinput/touchpad.qml:625
#, kde-format
msgid "Touchpad edges"
msgstr "Okraje touchpadu"

#: kcm/libinput/touchpad.qml:629
#, kde-format
msgid "Slide on the touchpad edges scrolls."
msgstr "Posun prstů po okraji touchpadu roluje obrazovku."

#: kcm/libinput/touchpad.qml:639
#, kde-format
msgid "Invert scroll direction (Natural scrolling)"
msgstr "Obrátit směr rolování (tzv. přirozený směr rolování)"

#: kcm/libinput/touchpad.qml:655
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Rolování jako na dotykové obrazovce."

#: kcm/libinput/touchpad.qml:663 kcm/libinput/touchpad.qml:680
#, kde-format
msgid "Disable horizontal scrolling"
msgstr "Vypnout vodorovné rolování"

#: kcm/libinput/touchpad.qml:688
#, kde-format
msgid "Scrolling speed:"
msgstr "Rychlost rolování:"

#: kcm/libinput/touchpad.qml:739
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Pomaleji"

#: kcm/libinput/touchpad.qml:745
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Rychleji"

#: kcm/libinput/touchpad.qml:755
#, kde-format
msgid "Right-click:"
msgstr "Kliknutí pravým tlačítkem myši:"

#: kcm/libinput/touchpad.qml:789
#, kde-format
msgid "Press bottom-right corner"
msgstr "Stiskněte pravý dolní roh"

#: kcm/libinput/touchpad.qml:793
#, kde-format
msgid ""
"Software enabled buttons will be added to bottom portion of your touchpad."
msgstr "Tlačítka povolená softwarem budou přidána do spodní části touchpadu."

#: kcm/libinput/touchpad.qml:801
#, kde-format
msgid "Press anywhere with two fingers"
msgstr "Stisknout kdekoliv dvěma prsty"

#: kcm/libinput/touchpad.qml:805
#, kde-format
msgid "Tap with two finger to enable right click."
msgstr "Ťukněte dvěma prsty, chcete-li povolit kliknutí pravým tlačítkem."

#: kcm/libinput/touchpad.qml:819
#, kde-format
msgid "Middle-click: "
msgstr "Kliknutí prostředním tlačítkem myši: "

#: kcm/libinput/touchpad.qml:848
#, kde-format
msgid "Press bottom-middle"
msgstr "Stiskněte uprostřed dole"

#: kcm/libinput/touchpad.qml:852
#, kde-format
msgid ""
"Software enabled middle-button will be added to bottom portion of your "
"touchpad."
msgstr ""
"Prostřední tlačítko povolené softwarem bude přidáno do spodní části "
"touchpadu."

#: kcm/libinput/touchpad.qml:860
#, kde-format
msgid "Press bottom left and bottom right corners simultaneously"
msgstr "Stiskněte pravý a levý dolní roh zároveň"

#: kcm/libinput/touchpad.qml:873
#, kde-format
msgid "Press anywhere with three fingers"
msgstr "Stisknout kdekoliv třemi prsty"

#: kcm/libinput/touchpad.qml:879
#, kde-format
msgid "Press anywhere with three fingers."
msgstr "Stiskněte kdekoliv třemi prsty."

#: kcm/touchpadconfig.cpp:99
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"Chyba při načítání hodnot.Více informací naleznete v záznamech činnosti. "
"Prosím restartujte tento nastavovací modul."

#: kcm/touchpadconfig.cpp:102
#, kde-format
msgid "No touchpad found. Connect touchpad now."
msgstr "Nebyl nalezen žádný touchpad. Připojte touchpad."

#: kcm/touchpadconfig.cpp:111
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Nebylo možné uložit všechny změny. Více informací naleznete v záznamu. "
"Restartujte tento konfigurační modul a zkuste to znovu."

#: kcm/touchpadconfig.cpp:130
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"Chyba při načítání výchozích hodnot. Nastavení některých voleb na výchozí "
"hodnotu selhalo."

#: kcm/touchpadconfig.cpp:150
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"Při přidávání nově připojeného zařízení došlo k chybě. Připojte zařízení "
"znovu a restartujte tento konfigurační modul."

#: kcm/touchpadconfig.cpp:173
#, kde-format
msgid "Touchpad disconnected. Closed its setting dialog."
msgstr "Touchpad byl odpojen. Jeho nastavovací dialog byl uzavřen."

#: kcm/touchpadconfig.cpp:175
#, kde-format
msgid "Touchpad disconnected. No other touchpads found."
msgstr "Touchpad odpojen. Žádné další touchpady nebyly nalezeny."

#: kded/kded.cpp:201
#, kde-format
msgid "Touchpad was disabled because a mouse was plugged in"
msgstr "Touchpad byl zakázán protože byla připojena myš"

#: kded/kded.cpp:204
#, kde-format
msgid "Touchpad was enabled because the mouse was unplugged"
msgstr "Touchpad byl povolen protože byla odpojena myš"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:27
#, kde-format
msgctxt "Emulated mouse button"
msgid "No action"
msgstr "Žádná činnost"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:30
#, kde-format
msgctxt "Emulated mouse button"
msgid "Left button"
msgstr "Levé tlačítko"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:33
#, kde-format
msgctxt "Emulated mouse button"
msgid "Middle button"
msgstr "Prostřední tlačítko"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:36
#, kde-format
msgctxt "Emulated mouse button"
msgid "Right button"
msgstr "Pravé tlačítko"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:285
#, kde-format
msgctxt "Touchpad Edge"
msgid "All edges"
msgstr "Všechny okraje"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:288
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top edge"
msgstr "Horní okraj"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:291
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top right corner"
msgstr "Pravý horní roh"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:294
#, kde-format
msgctxt "Touchpad Edge"
msgid "Right edge"
msgstr "Pravý okraj"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:297
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom right corner"
msgstr "Pravý dolní roh"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:300
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom edge"
msgstr "Spodní okraj"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:303
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom left corner"
msgstr "Levý dolní roh"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:306
#, kde-format
msgctxt "Touchpad Edge"
msgid "Left edge"
msgstr "Levý okraj"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:309
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top left corner"
msgstr "Levý horní roh"
