# Translation of kcmkeyboard.po into Finnish
# Copyright © 2007, 2008, 2010, 2011, 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the kde-workspace package.
# Mikko Piippo <piippo@cc.helsinki.fi>, 2007.
# Teemu Rytilahti <teemu.rytilahti@kde-fi.org>, 2008.
# Teemu Rytilahti <teemu.rytilahti@d5k.net>, 2008.
# Lasse Liehu <lasse.liehu@gmail.com>, 2010, 2011, 2012, 2013, 2014, 2016, 2017.
# Jiri Grönroos <jiri.gronroos+kde@iki.fi>, 2012.
# Tommi Nieminen <translator@legisign.org>, 2012, 2020, 2021, 2023.
#
# KDE Finnish translation sprint participants:
#
# KDE Finnish translation sprint participants:
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-30 01:42+0000\n"
"PO-Revision-Date: 2023-03-13 22:22+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:22:22+0000\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tommi Nieminen,Mikko Piippo, Teemu Rytilahti, Lasse Liehu"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"translator@legisign.org,piippo@cc.helsinki.fi, teemu.rytilahti@kde-fi.org, "
"lasse.liehu@gmail.com"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr "Näppäinasettelun vaihto"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Vaihda seuraavaan näppäinasetteluun"

#: bindings.cpp:30
#, fuzzy, kde-format
#| msgid "Switch to Next Keyboard Layout"
msgid "Switch to Last-Used Keyboard Layout"
msgstr "Vaihda seuraavaan näppäinasetteluun"

# Lainausmerkit auttavat, koska %1 alkaa isolla alkukirjaimella (esim. ”Venäläinen”)
#: bindings.cpp:60
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Vaihda näppäinasetteluksi ”%1”"

#: flags.cpp:77
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr "%1 – %2"

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, kde-format
msgid "Add Layout"
msgstr "Lisää asettelu"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr "Etsi…"

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr "Pikanäppäin:"

# Aiemmin tässä tiedostossa sekä ”nimike” että ”tunniste”, nyt yhtenäistetty -> nimiö (aika ylimalkainen sana, mutta sopii käyttötarkoitukseensa)
#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "Nimiö:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:338
#, kde-format
msgid "Preview"
msgstr "Esikatselu"

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:18
#, kde-format
msgid "Hardware"
msgstr "Laitteisto"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:33
#, kde-format
msgid "Keyboard &model:"
msgstr "Näppäimistön &malli:"

# ”104-key” on tietysti amerikkalainen muunnelma, joten ehkä käännöksessä voisi suoraan suositella 105-näppäimistäkin?
#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:53
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""
"Näppäimistön mallin voi asettaa tässä. Asetus ei riipu näppäinasettelusta ja "
"viittaa väljästi laitteistomalliin. Nykyaikaisissa tietokoneiden mukana "
"tulevissa näppäimistöissä on yleensä kaksi tai kolme ylimääräistä näppäintä, "
"ja niitä kutsutaan ”104/105-näppäimisiksi” malleiksi. Ellet tiedä "
"näppäimistösi mallia, haluat luultavasti tämän.\n"

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:94
#, kde-format
msgid "Layouts"
msgstr "Asettelut"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:102
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"Jos käyttöalaksi valitsee ”Sovellus” tai ”Ikkuna”, näppäinasettelun vaihto "
"vaikuttaa vain nykyiseen sovellukseen tai ikkunaan."

# Vanha ”vaihtamiskäytäntö” jää epäselväksi: kyse on näppäimistön vaihdon käyttöalasta
#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid "Switching Policy"
msgstr "Vaihdon käyttöala"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:111
#, kde-format
msgid "&Global"
msgstr "&Järjestelmä"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:124
#, kde-format
msgid "&Desktop"
msgstr "&Työpöytä"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:134
#, kde-format
msgid "&Application"
msgstr "&Sovellus"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:144
#, kde-format
msgid "&Window"
msgstr "&Ikkuna"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:157
#, kde-format
msgid "Shortcuts for Switching Layout"
msgstr "Asettelun vaihdon pikanäppäimet"

# Suomennokseen valittu tarkoituksella yksikkö
#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:163
#, kde-format
msgid "Main shortcuts:"
msgstr "Ensisijainen pikanäppäin:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:176
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""
"X.orgin käsittelemät asettelunvaihdon pikanäppäimet. Pikanäppäimiksi "
"sallitaan vain muunnosnäppäimet."

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:179 kcm_keyboard.ui:209
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr "Ei mikään"

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:186 kcm_keyboard.ui:216
#, kde-format
msgid "…"
msgstr "…"

# Suomennokseen valittu tarkoituksella yksikkö
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:193
#, kde-format
msgid "3rd level shortcuts:"
msgstr "Kolmannen tason pikanäppäin:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:206
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""
"Tällä X.orgin pikanäppäimellä siirrytään aktiivisen näppäimistöasettelun "
"kolmannelle tasolle. Vain muunnosnäppäimiä voi käyttää pikanäppäiminä."

# Id on outo: näppäin, jolla vaihdetaan näppäimistöasettelua (oletuksena Ctrl-Alt-K)
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:223
#, kde-format
msgid "Alternative shortcut:"
msgstr "Asettelunvaihtonäppäin:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:236
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""
"Tällä pikanäppäimellä vaihdetaan näppäimistöasettelua. Pelkän "
"muunnosnäppäimen käyttöä pikanäppäimenä ei tueta eikä näppäinyhdistelmä "
"välttämättä toimi kaikissa tilanteissa (esim. ponnahdusikkunan ollessa "
"aktiivinen tai näytönsäästäjässä)."

# Id on outo: näppäin, jolla vaihdetaan näppäimistöasettelua (oletuksena Ctrl-Alt-K)
#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm_keyboard.ui:246
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgid "Last used shortcut:"
msgstr "Asettelunvaihtonäppäin:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, toggleLastUsedLayoutKeySequence)
#: kcm_keyboard.ui:259
#, kde-format
msgid ""
"This shortcut allows for fast switching between two layouts, by always "
"switching to the last-used one."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:284
#, kde-format
msgid "Configure layouts"
msgstr "Valitse asettelut"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:298
#, kde-format
msgid "Add"
msgstr "Lisää"

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:308
#, kde-format
msgid "Remove"
msgstr "Poista"

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:318
#, kde-format
msgid "Move Up"
msgstr "Siirrä ylemmäs"

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:328
#, kde-format
msgid "Move Down"
msgstr "Siirrä alemmas"

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:373
#, kde-format
msgid "Spare layouts"
msgstr "Ylimääräiset asettelut"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:405
#, kde-format
msgid "Main layout count:"
msgstr "Pääasettelujen määrä:"

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:435
#, kde-format
msgid "Advanced"
msgstr "Lisäasetukset"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:441
#, kde-format
msgid "&Configure keyboard options"
msgstr "&Muokkaa näppäimistövalintoja"

#: kcm_keyboard_widget.cpp:209
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Tuntematon"

#: kcm_keyboard_widget.cpp:211
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: kcm_keyboard_widget.cpp:655
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Ei asetettu"

#: kcm_keyboard_widget.cpp:669
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 pikanäppäin"
msgstr[1] "%1 pikanäppäintä"

# Vastaa kcmkeyboard.po-tiedoston käytäntöä: lyhyt (aina?) kaksikirjaiminen jono, joka kuvaa asettelun
#: kcm_view_models.cpp:200
#, kde-format
msgctxt "layout map name"
msgid "Map"
msgstr "Nimiö"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "Asettelu"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "Muunnelma"

# Outo käännösvastine, mutta tämän alle näppäimistöasettelujen ikkunassa tulee yleensä lipun kuva tai vapaavalintainen 1-3-kirjaiminen teksti
#: kcm_view_models.cpp:200
#, kde-format
msgid "Label"
msgstr "Tunnus"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Shortcut"
msgstr "Pikanäppäin"

#: kcm_view_models.cpp:273
#, kde-format
msgctxt "variant"
msgid "Default"
msgstr "Oletus"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr "Kun näppäintä pidetään pohjassa:"

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr "Näytä &lisämerkit"

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr "T&oista näppäintä"

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr "&Älä tee mitään"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr "Kokeilualue:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""
"Mahdollistaa näppäintoiston ja näppäilyäänen voimakkuuden kokeilun (mutta "
"älä unohda ottaa muutoksia käyttöön!)."

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, kde-format
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""
"Jos tuettu, valinta antaa määrittää, onko numerolukko asetettu Plasman "
"käynnistyttyä.<p>Numerolukon tilaksi voi valita sekä ”käytössä” että ”ei "
"käytössä” tai määrätä, ettei Plasma muuta numerolukon tilaa."

#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, kde-format
msgid "NumLock on Plasma Startup"
msgstr "Numerolukko Plasman käynnistyessä"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "Ota &käyttöön"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, kde-format
msgid "&Turn off"
msgstr "P&oista käytöstä"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "&Älä muuta tilaa"

# HUOM! En mahda mitään, mutta minusta ”tahti” ei tässä vain kuulosta oikealta
#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "&Nopeus:"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, kde-format
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"Jos tuettu, valinta antaa sinun viiveen, jonka kuluttua alhaalla pidetyn "
"näppäimen toisto käynnistyy. ”Nopeus” määrää toiston nopeuden."

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"Jos tuettu, valinta antaa määrittää nopeuden, jolla näppäintä toistetaan."

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr " toistoa/s"

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr " ms"

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "&Viive"

#: tastenbrett/main.cpp:52
#, kde-format
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Näppäimistön esikatselu"

#: tastenbrett/main.cpp:54
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Näppäimistöasettelun yleiskuva"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""
"Näppäimistögeometrian lataaminen epäonnistui. Tämä osoittaa yleensä, ettei "
"valittu malli tue valittua asettelua tai sen muunnelmaa. Odotettavissa voi "
"olla ongelmia, jos käytät tätä mallin, asettelun ja muunnelman yhdistelmää."

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "KDE:n näppäimistöasetukset"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "© 2010 Andriy Rysin"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Näppäimistö</h1> Tästä voit vaihtaa näppäimistöasettelun sekä muita "
#~ "asetuksia."

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "KDE:n näppäinasettelun vaihto"

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Vain korkeintaan %1 näppäimistöasettelun käyttöä tuetaan"
#~ msgstr[1] "Vain korkeintaan %1 näppäimistöasettelun käyttöä tuetaan"

#~ msgid "Any language"
#~ msgstr "Mikä tahansa kieli"

#~ msgid "Layout:"
#~ msgstr "Asettelu:"

#~ msgid "Variant:"
#~ msgstr "Muunnelma:"

#~ msgid "Limit selection by language:"
#~ msgstr "Rajaa valintaa kielen perusteella:"

#~ msgid "..."
#~ msgstr "…"

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 – %2"

#~ msgid "Layout Indicator"
#~ msgstr "Asettelun ilmaisin"

#~ msgid "Show layout indicator"
#~ msgstr "Näytä asettelun ilmaisin"

#~ msgid "Show for single layout"
#~ msgstr "Näytä yhdelle asettelulle"

#~ msgid "Show flag"
#~ msgstr "Näytä lippu"

#~ msgid "Show label"
#~ msgstr "Näytä nimiö"

#~ msgid "Show label on flag"
#~ msgstr "Näytä nimiö lipussa"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Näppäinasettelu"

#~ msgid "Configure Layouts..."
#~ msgstr "Valitse asettelut…"

#~ msgid "Keyboard Repeat"
#~ msgstr "Näppäintoisto"

#~ msgid "Turn o&ff"
#~ msgstr "P&oista käytöstä"

#~ msgid "&Leave unchanged"
#~ msgstr "&Älä muuta tilaa"

#~ msgid "Configure..."
#~ msgstr "Asetukset…"

#~ msgid "Key Click"
#~ msgstr "Näppäilyääni"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "Jos tuettu, tämä valinta ottaa käyttöön tietokoneen kaiuttimista "
#~ "soitettavan näppäimistöäänen. Näppäimistöääni voi olla hyödyksi, jos "
#~ "tietokoneessa ei ole mekaanista näppäimistöä tai jos näppäimistö on hyvin "
#~ "hiljainen. <p> Voit muuttaa äänen voimakkuutta liukuvalitsimella tai "
#~ "napsauttamalla nuolipainikkeita. Aseta voimakkuus 0%:ksi, jos haluat "
#~ "poistaa äänen käytöstä."

#~ msgid "&Key click volume:"
#~ msgstr "Näppäilyäänen &voimakkuus:"

#~ msgid "No layout selected "
#~ msgstr "Asettelua ei ole valittu "

#~ msgid "XKB extension failed to initialize"
#~ msgstr "XKB-laajennuksen alustus epäonnistui"

#~ msgid "Backspace"
#~ msgstr "Askelpalautin"

#~ msgctxt "Tab key"
#~ msgid "Tab"
#~ msgstr "Sarkain"

#~ msgid "Caps Lock"
#~ msgstr "Vaihtolukko"

#~ msgid "Enter"
#~ msgstr "Enter"

#~ msgid "Ctrl"
#~ msgstr "Ctrl"

#~ msgid "Alt"
#~ msgstr "Alt"

#~ msgid "AltGr"
#~ msgstr "AltGr"

#~ msgid "Esc"
#~ msgstr "Esc"

#~ msgctxt "Function key"
#~ msgid "F%1"
#~ msgstr "F%1"

#~ msgid "Shift"
#~ msgstr "Vaihto"

#~ msgid "No preview found"
#~ msgstr "Esikatselua ei löydy"

#~ msgid "Close"
#~ msgstr "Sulje"

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "Jos valitset tämän, näppäimen pitäminen alhaalla lähettää saman merkin "
#~ "yhä uudestaan ja uudestaan. Jos esimerkiksi pidät alhaalla Sarkain-"
#~ "näppäintä, toimitaan samalla tavoin kuin jos painaisit Sarkainta useita "
#~ "kertoja peräkkäin: Sarkain-merkkejä lähetetään kunnes vapautat näppäimen."

#~ msgid "&Enable keyboard repeat"
#~ msgstr "&Ota näppäintoisto käyttöön"
