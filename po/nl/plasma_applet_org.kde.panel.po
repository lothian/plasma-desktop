# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2018, 2019, 2021, 2022, 2023 Freek de Kruijf <freekdekruijf@kde.nl>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-19 01:34+0000\n"
"PO-Revision-Date: 2023-11-14 10:25+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#: contents/ui/ConfigOverlay.qml:277 contents/ui/ConfigOverlay.qml:311
#, kde-format
msgid "Remove"
msgstr "Verwijderen"

#: contents/ui/ConfigOverlay.qml:287
#, kde-format
msgid "Configure…"
msgstr "Configureren…"

#: contents/ui/ConfigOverlay.qml:298
#, kde-format
msgid "Show Alternatives…"
msgstr "Alternatieven tonen…"

#: contents/ui/ConfigOverlay.qml:321
#, kde-format
msgid "Spacer width"
msgstr "Breedte van scheiding"

#: contents/ui/main.qml:388
#, kde-format
msgid "Add Widgets…"
msgstr "Widgets toevoegen…"
