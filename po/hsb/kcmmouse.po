# translation of kcminput.po to Hornjoserbsce
# Eduard Werner <edi.werner@gmx.de>, 2005, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-13 01:35+0000\n"
"PO-Revision-Date: 2008-11-13 22:59+0100\n"
"Last-Translator: Eduard Werner <edi.werner@gmx.de>\n"
"Language-Team: en_US <kde-i18n-doc@lists.kde.org>\n"
"Language: hsb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Edward Wornar"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "edi.werner@gmx.de"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:100
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:105
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:116
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:136
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:158
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:182
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:184
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:73
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:97 kcm/libinput/main_deviceless.qml:50
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "&Powšitkownje"

#: kcm/libinput/main.qml:98
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:118
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:123 kcm/libinput/main_deviceless.qml:51
#, fuzzy, kde-format
#| msgid "Le&ft handed"
msgid "Left handed mode"
msgstr "za &lěwu ruku"

#: kcm/libinput/main.qml:143 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:148 kcm/libinput/main_deviceless.qml:76
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:168 kcm/libinput/main_deviceless.qml:96
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:178 kcm/libinput/main_deviceless.qml:106
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Pointer speed:"
msgstr "Minimalna distanca:"

#: kcm/libinput/main.qml:210 kcm/libinput/main_deviceless.qml:138
#, kde-format
msgid "Pointer acceleration:"
msgstr "Pospěšenje pokazowaka:"

#: kcm/libinput/main.qml:241 kcm/libinput/main_deviceless.qml:169
#, kde-format
msgid "None"
msgstr ""

#: kcm/libinput/main.qml:245 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:251 kcm/libinput/main_deviceless.qml:179
#, kde-format
msgid "Standard"
msgstr ""

#: kcm/libinput/main.qml:255 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:267 kcm/libinput/main_deviceless.qml:195
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:268 kcm/libinput/main_deviceless.qml:196
#, fuzzy, kde-format
#| msgid "Re&verse scroll direction"
msgid "Invert scroll direction"
msgstr "směr suwanja wob&roćić"

#: kcm/libinput/main.qml:284 kcm/libinput/main_deviceless.qml:212
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:289
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Scrolling speed:"
msgstr "Minimalna distanca:"

#: kcm/libinput/main.qml:339
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:345
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:355
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:393
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:423
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:424
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:428
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:445
#, fuzzy, kde-format
#| msgid "Press Connect Button"
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Stłóčće na knefl za zwisk"

#: kcm/libinput/main.qml:446
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:475
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#~ msgid ""
#~ "<h1>Mouse</h1> This module allows you to choose various options for the "
#~ "way in which your pointing device works. Your pointing device may be a "
#~ "mouse, trackball, or some other hardware that performs a similar function."
#~ msgstr ""
#~ "<h1>Myš</h1> Tutón modul dowoli Wam wšelake opcije wubrać za Waš "
#~ "pokazowadło. Pokazowadło móže być myš, trackball abo druhi grat z "
#~ "podobnej funkciju."

#~ msgid "&General"
#~ msgstr "&Powšitkownje"

#~ msgid ""
#~ "If you are left-handed, you may prefer to swap the functions of the left "
#~ "and right buttons on your pointing device by choosing the 'left-handed' "
#~ "option. If your pointing device has more than two buttons, only those "
#~ "that function as the left and right buttons are affected. For example, if "
#~ "you have a three-button mouse, the middle button is unaffected."
#~ msgstr ""
#~ "Hdyž sće lěwicar, chceće snadź radšo funkcije lěweho a praweho myšaceho "
#~ "knefla zaměnić. Ma-li waša myš wjace hač dwaj kneflej, budźe to jenož tej "
#~ "kneflej potrjechić, kiž fungujetej jako lěwy a prawy knefl."

#~ msgid "Button Order"
#~ msgstr " Rjad kneflow"

#~ msgid "Righ&t handed"
#~ msgstr "za &prawu ruku"

#~ msgid "Le&ft handed"
#~ msgstr "za &lěwu ruku"

#~ msgid "Re&verse scroll direction"
#~ msgstr "směr suwanja wob&roćić"

#~ msgid "Advanced"
#~ msgstr "Za pokročenych"

#~ msgid "Pointer threshold:"
#~ msgstr "Minimalna distanca:"

#~ msgid "Double click interval:"
#~ msgstr "Interwal za dwójny klik:"

#~ msgid "Mouse wheel scrolls by:"
#~ msgstr "Myšace koleso přesuwa wo:"

#~ msgid " x"
#~ msgstr " x"

#, fuzzy
#~ msgid ""
#~ "<p>The threshold is the smallest distance that the mouse pointer must "
#~ "move on the screen before acceleration has any effect. If the movement is "
#~ "smaller than the threshold, the mouse pointer moves as if the "
#~ "acceleration was set to 1X;</p><p> thus, when you make small movements "
#~ "with the physical device, there is no acceleration at all, giving you a "
#~ "greater degree of control over the mouse pointer. With larger movements "
#~ "of the physical device, you can move the mouse pointer rapidly to "
#~ "different areas on the screen.</p>"
#~ msgstr "<p> ně."

#~ msgid " msec"
#~ msgstr " msek"

#, fuzzy
#~| msgid "Mouse Navigation"
#~ msgid "Keyboard Navigation"
#~ msgstr "Nawigaciju z myšu"

#~ msgid "&Move pointer with keyboard (using the num pad)"
#~ msgstr "&Pokazowak z tastaturu pohibować (z ličbami)"

#~ msgid "R&epeat interval:"
#~ msgstr "Interwal za &wospjetowanje:"

#~ msgid "Ma&ximum speed:"
#~ msgstr "Maksimalna &spěšnosć:"

#~ msgid " pixel/sec"
#~ msgstr "pixle/sek"

#~ msgid "Acceleration &profile:"
#~ msgstr "Profil pos&pěšenja:"

#~ msgid " pixel"
#~ msgid_plural " pixels"
#~ msgstr[0] " dypk"
#~ msgstr[1] " dypkaj"
#~ msgstr[2] " dypki"
#~ msgstr[3] " dypkow"

#~ msgid " line"
#~ msgid_plural " lines"
#~ msgstr[0] " linka"
#~ msgstr[1] " lince"
#~ msgstr[2] " linki"
#~ msgstr[3] " linkow"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration profile:"
#~ msgstr "Profil pos&pěšenja:"

#~ msgid "Mouse"
#~ msgstr "Myš"

#, fuzzy
#~| msgid "(c) 1997 - 2005 Mouse developers"
#~ msgid "(c) 1997 - 2018 Mouse developers"
#~ msgstr "(c) 1997 - 2005 Mouse wuwiwarjo"

#~ msgid "Patrick Dowler"
#~ msgstr "Patrick Dowler"

#~ msgid "Dirk A. Mueller"
#~ msgstr "Dirk A. Mueller"

#~ msgid "David Faure"
#~ msgstr "David Faure"

#~ msgid "Bernd Gehrmann"
#~ msgstr "Bernd Gehrmann"

#~ msgid "Rik Hemsley"
#~ msgstr "Rik Hemsley"

#~ msgid "Brad Hughes"
#~ msgstr "Brad Hughes"

#~ msgid "Brad Hards"
#~ msgstr "Brad Hards"

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration:"
#~ msgstr "Profil pos&pěšenja:"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "Profil pos&pěšenja:"

#~ msgid "Icons"
#~ msgstr "Piktogramy"

#~ msgid ""
#~ "The default behavior in KDE is to select and activate icons with a single "
#~ "click of the left button on your pointing device. This behavior is "
#~ "consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, check this option."
#~ msgstr ""
#~ "Standard w KDE je wuběranje a aktiwizowanje piktograma z jednorym "
#~ "kliknjenjom lěweho myšaceho knefla. To je konsistentne ze zadźerženjom, "
#~ "kiž dóstanjeće, hdyž na wotkaz we www-stronje kliknjeće. Jeli chceće z "
#~ "jednorym kliknjenjom jenož wuběrać a z dwójnym aktiwizować (kaž w MS-"
#~ "windows), wubjerće tutu opciju."

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "Dwójny &klik za wočinjenje datajow a zapiskow (prěni klik wubjerje "
#~ "piktogramy)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "Aktiwizuje a wočini zapisk z jeničkim kliknjenjom."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "&jenički klik wočini dataje a zapiski"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "Wubjerće cursorowu temu, kotruž chceće wužiwać:"

#~ msgid "Name"
#~ msgstr "Mjeno"

#~ msgid "Description"
#~ msgstr "Wopisanje"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr "Dyrbiće KDE znowa startować, zo bychu so tute změny wuskutkowali."

#~ msgid "Cursor Settings Changed"
#~ msgstr "Nastajenja za cursor změnjene"

#~ msgid "Small black"
#~ msgstr "mały čorny"

#~ msgid "Small black cursors"
#~ msgstr "małe čorne cursory"

#~ msgid "Large black"
#~ msgstr "wulke čorne"

#~ msgid "Large black cursors"
#~ msgstr "wulke čorne cursory"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "Pokazowak &změnić na piktogramach"

#~ msgid "A&utomatically select icons"
#~ msgstr "Piktogramy &awtomatisce wubrać"

#, fuzzy
#~| msgid "Dela&y:"
#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "Inter&wal:"

#, fuzzy
#~| msgid " msec"
#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " msek"

#~ msgid ""
#~ "If you check this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."
#~ msgstr ""
#~ "Hdyž wubjerjeć tutu opciju, so piktogram awtomatisce wubjerje, hdyž myš "
#~ "na nim stejo wostanje. To móže być wužitne, hdyž jenički klik piktogram "
#~ "aktiwizuje, a chceće piktogram jenož wubrać, bjeztoho zo so aktiwizuje."

#~ msgid ""
#~ "If you have checked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."
#~ msgstr ""
#~ "Hdyž sće opciju za awtomatiske wuběranje piktogramow wubrali, dowoli wam "
#~ "tutón suwak nastajić, kak dołho dyrbi myš na piktogramje stejo wostać, "
#~ "prjedy hač so wubjerje."

#~ msgid "Mouse type: %1"
#~ msgstr "Družina myše: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF-kanal 1 nastajeny. Prošu stłóčće knefl na myšce za znowanastajenje "
#~ "zwiska."

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF-kanal 2 nastajeny.  Prošu stłóčće knefl na myšce za znowanastajenje "
#~ "zwiska."

#, fuzzy
#~| msgid "none"
#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "žana"

#~ msgid "Cordless Mouse"
#~ msgstr "myška bjez kabla"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "Kolesata myška bjez kabla"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "MouseMan Wheel bjez kabla"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "TrackMan Wheel bjez kabla"

#~ msgid "TrackMan Live"
#~ msgstr "TrackMan Live"

#~ msgid "Cordless TrackMan FX"
#~ msgstr "TrackMan FX bjez kabla"

#~ msgid "Cordless MouseMan Optical"
#~ msgstr "MouseMan Optical bjez kabla"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "optiska myš bjez kabla"

#~ msgid "Cordless MouseMan Optical (2ch)"
#~ msgstr "optiski MouseMan bjez kabla (2 kanalej)"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "optiska myš bjez kabla (2 kanalej)"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "Bjezkablowa myš (2 kanalej)"

#~ msgid "Cordless Optical TrackMan"
#~ msgstr "Optiski TrackMan bjez kabla"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "MX700 bjezkablowa optiska myš"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "MX700 bjezkablowa optiska myš (2 kanalej)"

#~ msgid "Unknown mouse"
#~ msgstr "Njeznata myš"

#~ msgid "Cordless Name"
#~ msgstr "Mjeno bjezkabloweje myše"

#~ msgid "Sensor Resolution"
#~ msgstr "Rozpušćenje sensora"

#~ msgid "400 counts per inch"
#~ msgstr "400/inch"

#~ msgid "800 counts per inch"
#~ msgstr "800/inch"

#~ msgid "Battery Level"
#~ msgstr "Staw baterije"

#~ msgid "RF Channel"
#~ msgstr "RF-kanal"

#~ msgid "Channel 1"
#~ msgstr "Kanal 1"

#~ msgid "Channel 2"
#~ msgstr "Kanal 2"

#~ msgid ""
#~ "You have a Logitech Mouse connected, and libusb was found at compile "
#~ "time, but it was not possible to access this mouse. This is probably "
#~ "caused by a permissions problem - you should consult the manual on how to "
#~ "fix this."
#~ msgstr ""
#~ "Maće Logitech myšku a libusb je instalowane, ale njeje móžno myš "
#~ "skontaktować. To najskerje na přistupnych prawach zaleži. Prošu "
#~ "pohladajće do přiručki po dalšu informaciju."

#, fuzzy
#~| msgid "&Cursor Theme"
#~ msgid "Cursor Theme"
#~ msgstr "&Cursorowa tema"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr "Wubjerće cursorowu temu, kotruž chceće wužiwać:"

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Get New Theme..."
#~ msgstr "Nowu temu instalować..."

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Install From File..."
#~ msgstr "Nowu temu instalować..."

#~ msgid "Remove Theme"
#~ msgstr "Temu wumaznyć"

#~ msgid "Short"
#~ msgstr "krótki"

#~ msgid "Long"
#~ msgstr "dołhi"
