# translation of kcmkded.po to British English
# Copyright (C) 2002,2003, 2004, 2008 Free Software Foundation, Inc.
#
# Malcolm Hunter <malcolm.hunter@gmx.co.uk>, 2002,2003, 2008.
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2004, 2009, 2015.
# Steve Allewell <steve.allewell@gmail.com>, 2017, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-07 02:44+0000\n"
"PO-Revision-Date: 2020-09-26 14:37+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: kcmkded.cpp:115
#, kde-format
msgid "Failed to stop service: %1"
msgstr "Failed to stop service: %1"

#: kcmkded.cpp:117
#, kde-format
msgid "Failed to start service: %1"
msgstr "Failed to start service: %1"

#: kcmkded.cpp:124
#, kde-format
msgid "Failed to stop service."
msgstr "Failed to stop service."

#: kcmkded.cpp:126
#, kde-format
msgid "Failed to start service."
msgstr "Failed to start service."

#: kcmkded.cpp:224
#, fuzzy, kde-format
#| msgid "Failed to notify KDE Service Manager (kded5) of saved changed: %1"
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr "Failed to notify KDE Service Manager (kded5) of saved changed: %1"

#: ui/main.qml:37
#, fuzzy, kde-format
#| msgid ""
#| "The background services manager (kded5) is currently not running. Make "
#| "sure it is installed correctly."
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""
"The background services manager (kded5) is currently not running. Make sure "
"it is installed correctly."

#: ui/main.qml:46
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."

#: ui/main.qml:55
#, fuzzy, kde-format
#| msgid ""
#| "Some services were automatically started/stopped when the background "
#| "services manager (kded5) was restarted to apply your changes."
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""
"Some services were automatically started/stopped when the background "
"services manager (kded5) was restarted to apply your changes."

#: ui/main.qml:97
#, kde-format
msgid "All Services"
msgstr "All Services"

#: ui/main.qml:98
#, kde-format
msgctxt "List running services"
msgid "Running"
msgstr "Running"

#: ui/main.qml:99
#, kde-format
msgctxt "List not running services"
msgid "Not Running"
msgstr "Not Running"

#: ui/main.qml:136
#, kde-format
msgid "Startup Services"
msgstr "Startup Services"

#: ui/main.qml:137
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Load-on-Demand Services"

#: ui/main.qml:156
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr "Toggle automatically loading this service on startup"

#: ui/main.qml:199
#, kde-format
msgid "Not running"
msgstr "Not running"

#: ui/main.qml:200
#, kde-format
msgid "Running"
msgstr "Running"

#: ui/main.qml:218
#, kde-format
msgid "Stop Service"
msgstr "Stop Service"

#: ui/main.qml:218
#, kde-format
msgid "Start Service"
msgstr "Start Service"

#~ msgid ""
#~ "<p>This module allows you to have an overview of all plugins of the KDE "
#~ "Daemon, also referred to as KDE Services. Generally, there are two types "
#~ "of service:</p> <ul><li>Services invoked at startup</li><li>Services "
#~ "called on demand</li></ul> <p>The latter are only listed for convenience. "
#~ "The startup services can be started and stopped. You can also define "
#~ "whether services should be loaded at startup.</p> <p><b>Use this with "
#~ "care: some services are vital for Plasma; do not deactivate services if "
#~ "you  do not know what you are doing.</b></p>"
#~ msgstr ""
#~ "<p>This module allows you to have an overview of all plugins of the KDE "
#~ "Daemon, also referred to as KDE Services. Generally, there are two types "
#~ "of service:</p> <ul><li>Services invoked at startup</li><li>Services "
#~ "called on demand</li></ul> <p>The latter are only listed for convenience. "
#~ "The startup services can be started and stopped. You can also define "
#~ "whether services should be loaded at startup.</p> <p><b>Use this with "
#~ "care: some services are vital for Plasma; do not deactivate services if "
#~ "you  do not know what you are doing.</b></p>"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Malcolm Hunter"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "malcolm.hunter@gmx.co.uk"

#~ msgid "Background Services"
#~ msgstr "Background Services"

#~ msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
#~ msgstr "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"

#~ msgid "Daniel Molkentin"
#~ msgstr "Daniel Molkentin"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "KDE Service Manager"

#~ msgid ""
#~ "This is a list of available KDE services which will be started on demand. "
#~ "They are only listed for convenience, as you cannot manipulate these "
#~ "services."
#~ msgstr ""
#~ "This is a list of available KDE services which will be started on demand. "
#~ "They are only listed for convenience, as you cannot manipulate these "
#~ "services."

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Description"
#~ msgstr "Description"

#~ msgid ""
#~ "This shows all KDE services that can be loaded on Plasma startup. Checked "
#~ "services will be invoked on next startup. Be careful with deactivation of "
#~ "unknown services."
#~ msgstr ""
#~ "This shows all KDE services that can be loaded on Plasma startup. Checked "
#~ "services will be invoked on next startup. Be careful with deactivation of "
#~ "unknown services."

#~ msgid "Use"
#~ msgstr "Use"

#~ msgid "Start"
#~ msgstr "Start"

#~ msgid "Stop"
#~ msgstr "Stop"

#~ msgid "Unable to contact KDED."
#~ msgstr "Unable to contact KDED."

#~ msgid "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"

#~ msgid "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
